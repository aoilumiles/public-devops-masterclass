resource "aws_internet_gateway" "public-gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "public gate way"
  }
}