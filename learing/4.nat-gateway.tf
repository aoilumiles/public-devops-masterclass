resource "aws_eip" "nat_eip" {
  vpc        = true
  depends_on = [aws_internet_gateway.public-gw]


  tags = {
    Name = "project-15-main-eip"
  }
}

resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.private_subnet_1a.id


  tags = {
    Name = "gw NAT"
  }

  depends_on = [aws_internet_gateway.public-gw]
}
resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.nat_eip.id

  subnet_id = aws_subnet.private_subnet_2a.id


  tags = {
    Name = "gw NAT"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.public-gw]
}

resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.nat_eip.id

  subnet_id = aws_subnet.private_subnet_1b.id


  tags = {
    Name = "gw NAT"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.public-gw]
}

resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.private_subnet_2b

  tags = {
    Name = "gw NAT"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.public-gw]
}