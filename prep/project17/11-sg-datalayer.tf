# security group for alb, to allow acess from any where for HTTP and HTTPS traffic
resource "aws_security_group" "datalayer-sg" {
  name        = "p15-datalayer-sg"
  vpc_id      = aws_vpc.main.id
  description = "Datalayer SG"


  tags = {
    Name            = "p15-datalayer-sg"
    Enviroment      = "production"
    Owner-Email     = "infradev@oldcowboyshop.com"
    Managed-By      = "Terraform"
    Billing-Account = "1234567890"
  }

}