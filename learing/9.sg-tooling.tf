resource "aws_security_group" "sg-tooling" {
  name        = "sg-tooling"
  description = "allow traffic to the webservers from ngnix"
  vpc_id      = aws_vpc.main.id

  tags = {
    "name" = "sg-tooling"
  }
}

resource "aws_security_group_rule" "sg-tooling-rule_from_Nginx" {
  type                     = "ingress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.nginx-sg.id
  security_group_id        = aws_security_group.sg-tooling.id
}
resource "aws_security_group_rule" "sg-tooling-rule_from_bastion" {
  type                     = "ingress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.bastion-sg.id
  security_group_id        = aws_security_group.sg-tooling.id
}