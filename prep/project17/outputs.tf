
################# Outputs ################
output "vpc_id" {
  value = aws_vpc.main.id
}


output "internet_gateway_id" {
  value = aws_internet_gateway.ig.id
}


output "external-alb-securtygroup_id" {
  value = aws_security_group.ext-alb-sg.id
}


output "RDS-endpoint" {
  value     = aws_db_instance.prj-15-RDS
  sensitive = true
}

output "EFS-filesystem-id" {
  value = aws_efs_file_system.ACS-efs
}